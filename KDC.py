import random
import socket
import keyexchange
import des
import time

if __name__ == "__main__":
    ks = '100101010'

    '''
    Diffie Hellman key exchange with bob and Alice
    '''
    Kbp = keyexchange.diffieHellmanKDC(int(ks,2))

    Kap = keyexchange.diffieHellmanKDC(int(ks,2))
    

    '''
    Send Alice the timestamp, session key, and bob's envelope
    '''
    s = socket.socket()
    port = 55555
    s.bind(('', port))
    s.listen(5)
    print('connected')
    while(True):
        c, addr = s.accept()
        print(addr)
        person = c.recv(1024)
        if(person == 'bob'):
            sessionKey = bin(random.randint(0,1000))[2:]

            while(len(sessionKey)<10):
                sessionKey += '0'
            #Session Key for Alice
            karr = []
            for i in range(0,len(sessionKey), 8):
                if(i+8>=len(sessionKey)):
                    kPad = sessionKey[i:]
                    while(len(kPad)<8):
                        kPad += '0'
                    karr.append(kPad)
                else:
                    karr.append(sessionKey[i:i+8])
            encryptedArr = []
            for i in karr:
                encryptedArr.append(''.join(str(e) for e in des.encrypt(i, Kap)))
            for i in encryptedArr:
                c.send(i)
                c.recv(1024)

            #Session Key for Bob
            encryptedArr = []
            for i in karr:
                encryptedArr.append(''.join(str(e) for e in des.encrypt(des.encrypt(i, Kbp), Kap)))
            for i in encryptedArr:
                c.send(i)
                c.recv(1024)
            
            #timestamp
            timestamp = bin(int(time.time()))[2:]
            while(len(timestamp) % 8 != 0):
                timestamp = '0' + timestamp
            timestampArr = []
            for i in range(0,len(timestamp), 8):
                timestampArr.append(timestamp[i:i+8])
            encryptedTimestampArr = []
            for i in timestampArr:
                encryptedTimestampArr.append(''.join(str(e) for e in des.encrypt(des.encrypt(i, Kbp), Kap)))
            for i in encryptedTimestampArr:
                c.send(i)
                c.recv(1024)
            c.send('done')
            s.close()
            break
        
