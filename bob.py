import socket
import des
import keyexchange
import random
import time

def diffie(s, kb):
    host = '127.0.0.1'
    port = 55555
    g = 12
    n = 1000
    s.connect((host,port))
    return keyexchange.diffieHellmanClient(s, g, n, int(kb,2))

if __name__ == "__main__":
    s = socket.socket()

    print('socket created')
    port = 12345

    kb = '1101010100'
    mac = '010101'
    '''
    This is where bob does a diffie hellman key exchance with KDC to send private key
    '''
    Kbp = bin(diffie(s, kb))[2:]

    #Waiting for Alice
    '''
    This is where bob waits for alice to send him his envelope and timestamp
    '''
    n = socket.socket()
    n.bind(('', 12345))
    n.listen(5)
    print('Listening for Alice...')
    while(True):
        c, addr = n.accept()
        print('Got connection from', addr)

        '''
        Bob decrypts the timestamp to make sure it's valied
        '''
        encryptedtimeArr = []
        while(True):
            t = c.recv(1024)
            if(t == 'done'):
                break
            else:
                encryptedtimeArr.append(t)
                c.send('a')
        timeArr = []
        for i in encryptedtimeArr:
            timeArr.append(des.decrypt(i, kb))
        timestamp = ''
        for i in timeArr:
            timestamp += ''.join(str(e) for e in i)
        timestamp = int(timestamp,2)
        newTimestamp = int(time.time())
        if(newTimestamp > timestamp + 3600): #After an hour the timestamp is considered expired
            print('too late of a session key')
            s.close()
        else:
            '''
            Bob Decrypts the envelope to get his copy of the session key
            '''
            encryptedEnvelope = []
            encryptedEnvelope.append(c.recv(1024))
            c.send('a')
            encryptedEnvelope.append(c.recv(1024))
            c.send('a')
            
            envelope = []
            for i in encryptedEnvelope:
                envelope.append(des.decrypt(i, kb))
            sessionKey = ''.join(str(e) for e in envelope[0])
            sessionKey += str(envelope[1][0]) + str(envelope[1][1])
 
            
            '''
            Bob generates a nonce for Alice to solve
            '''
            nonce = bin(random.randint(0,255))[2:]
            while(len(nonce)<8):
                nonce += '0'
            encryptedNonce = des.encrypt(nonce, sessionKey)
            encryptedNonceBin = ''.join(str(e) for e in encryptedNonce)
            c.send(encryptedNonceBin)
            newNonce = des.decrypt(c.recv(1024), sessionKey)
            newNonce = ''.join(str(e) for e in newNonce)
            if((int(newNonce, 2)+3 == int(nonce, 2))): #confirms the nonce
                print('valid!')
        
            s.close()
        break