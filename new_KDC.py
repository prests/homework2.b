#!/usr/bin/python3

import socket
import time
import des
import new_keyexchange as keyExchange
import numpy as np

if __name__ == '__main__':
    try:
        sessionKey = '1010101010'
        bobKey = '1111000000'
        binPrivateKey = '100101010'
        intPrivateKey = int(binPrivateKey,2)
        KDC = socket.socket()
        KDC.bind(('', 55555))
        KDC.listen(5)
        while True:
            c, addr = KDC.accept()
            print(addr)
            g = int(c.recv(1024).decode())
            c.send('ack'.encode())
            n = int(c.recv(1024).decode())
            c.send('ack'.encode())
            sharedKey = keyExchange.computationalDiffieHellman(c, g, n, intPrivateKey)

            sharedKey = bin(sharedKey)[2:]
            while(len(sharedKey)>10):
                print(sharedKey)
                sharedKey = sharedKey[1:]
            print(sharedKey)
        

            timestamp = bin(int(time.time()))[2:]
            print('Original timestamp: %s' %(timestamp))
            encryptedTimeStamp = ''
            print('timestamp: %s' %(timestamp))
            for i in range(0,int(len(timestamp)/8)):
                print('i: %s' %(i))
                tmpStrArr = np.array(list(timestamp[i*8:(i+1)*8]))
                tmpStrArr = des.encrypt(tmpStrArr, sharedKey)
                tmpStr = ''.join(str(x) for x in tmpStrArr)
                encryptedTimeStamp += tmpStr
            if(len(timestamp) % 8 != 0):
                pad = bin(len(timestamp)%8)[2:]
                tmpStr = timestamp[len(timestamp)-(len(timestamp)%8):]
                while(len(tmpStr)<8):
                    tmpStr += '0'
                tmpStrArr = np.array(list(tmpStr))
                print('here: %s' %(tmpStrArr))
                tmpStrArr = des.encrypt(tmpStrArr, sharedKey)
                tmpStr = ''.join(str(x) for x in tmpStrArr)
                encryptedTimeStamp += tmpStr
                print('what' + encryptedTimeStamp)

                while(len(pad)< 8):
                    pad = '0' + pad
                padArr = np.array(list(pad))
                print(padArr)
                encryptedPadArr = des.encrypt(padArr, sharedKey)
                encryptedPad = ''.join(str(x) for x in encryptedPadArr)
                print('encrypt'+encryptedPad)
                c.send(encryptedPad.encode())
                c.recv(3)
                print('Pad sent: %s' %(encryptedPad))
            else:
                tmp = des.encrypt(['0','0','0','0','0','0','0','0'], sharedKey)
                c.send((''.join(str(x) for x in tmp).encode()))
                c.recv(3)
            c.send(encryptedTimeStamp.encode())
            c.recv(3)
            print('timestamp sent: %s' %(encryptedTimeStamp))
            
            paddedSessionKey = sessionKey[8:] + '000000'
            paddedSessionKeyArr = np.array(list(paddedSessionKey))
            sessionKeyArr = np.array(list(sessionKey[:8]))
            aliceEnvelope = des.encrypt(sessionKeyArr, sharedKey)
            print('alice env: %s' %(aliceEnvelope))
            tmpStr = ''.join(str(x) for x in aliceEnvelope)
            aliceEnvelope = tmpStr
            c.send(aliceEnvelope.encode())
            c.recv(3)
            aliceEnvelope = des.encrypt(paddedSessionKeyArr, sharedKey)
            tmpStr = ''.join(str(x) for x in aliceEnvelope)
            aliceEnvelope = tmpStr
            c.send(aliceEnvelope.encode())
            c.recv(3)

            print('hererererere')
            bobEnvelope = des.encrypt(sessionKeyArr, bobKey)
            bobEnvelope = des.encrypt(bobEnvelope, sharedKey) 
            tmpStr = ''.join(str(x) for x in bobEnvelope)
            bobEnvelope = tmpStr
            c.send(bobEnvelope.encode())
            c.recv(3)
            bobEnvelope = des.encrypt(paddedSessionKeyArr, bobKey)
            bobEnvelope = des.encrypt(bobEnvelope, sharedKey)
            tmpStr = ''.join(str(x) for x in bobEnvelope)
            bobEnvelope = tmpStr
            c.send(bobEnvelope.encode())
            c.recv(3)
            
            KDC.close()
            break
    except KeyboardInterrupt:
        KDC.close()
