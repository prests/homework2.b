import socket
import des
import random

def calc(g,n,val): #Calculating the public part of Diffie Hellman
    return ((g**val)%n)

def calc2(g,n,val): #Public Key math
    return ((g**val)%n)

def diffieHellmanKDC(ks): #Diffie Hellman from KDC side
    s = socket.socket()
    port = 55555
    s.bind(('', port))
    s.listen(5)
    print('waiting for request')

    c, addr = s.accept()
    print('Got connection from', addr)
    print('Receiving...')

    #Diffie Hellman Key Exchange (Kac)
    g = int(c.recv(1024))
    c.send(g.encode())
    n = c.recv(1024).decode()
    c.send(n.encode())
    pubVal = calc(g,n,ks)
    pubRecv = int(c.recv(1024))
    c.send(pubVal.encode())
    
    Kac = calc2(pubRecv,n,ks)
    binKac = bin(Kac)[2:]
    while(len(binKac)<10):
        binKac += '0'

    encryptedArr = []
    encryptedArr.append(c.recv(1024).decode())
    c.send('a')
    encryptedArr.append(c.recv(1024).deocde())
    c.send('a')
    
    s.close()
    arr = []
    for i in encryptedArr:
        arr.append(des.decrypt(i, binKac))
    privKey = ''.join(str(e) for e in arr[0])
    privKey += str(arr[1][0]) + str(arr[1][1])
    return privKey

def diffieHellmanClient(s, g, n, k): #Diffie Hellman from Alice/Bob
    s.send(str(g))
    s.recv(1024)

    s.send(str(n))
    s.recv(1024)

    pubVal = calc(g,n,k)
    s.send(str(pubVal))
    pubRecv = int(s.recv(1024))
    secret = calc2(pubRecv,n,k)
    binSecret = bin(secret)[2:]
    #Padding 0's to make key fit length
    while(len(binSecret)<10):
        binSecret += '0'
    k = bin(k)[2:]
    #Adjucting k so it fits 8 bits
    karr = []
    for i in range(0,len(k), 8):
        if(i+8>=len(k)):
            kPad = k[i:]
            while(len(kPad)<8):
                kPad += '0'
            karr.append(kPad)
        else:
            karr.append(k[i:i+8])
    encryptedArr = []
    for i in karr:
        encryptedArr.append(''.join(str(e) for e in des.encrypt(i, binSecret)))
    for i in encryptedArr:
        s.send(i)
        s.recv(1024)
    s.close() 
    return secret
