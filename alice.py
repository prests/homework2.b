import socket               # Import socket module
import des
import sys
import keyexchange

s = socket.socket()         # Create a socket object
host = '127.0.0.1'          # Get local machine name
port = 55555                 # Reserve a port for your service.

ka = '1001010011'
mac = '100101'
g = 3
n = 1000

'''
This first part is the diffie Hellman key exchange with the KDC so Alice can send he private key to the KDC securely
'''
s.connect((host, port))
while(True):
    #Diffie Hellman Key Exchange with KDC (Kac)
    Kap = bin(keyexchange.diffieHellmanClient(s, g, n, int(ka,2)))[2:]
    break

raw_input('press enter') #without these manual breaks the code goes too fast and throws an error

'''
This part is where Alice gets a session key, an envelope for bob, and a timestamp that only bob will know
'''
s = socket.socket()

s.connect((host,port))
while(True):
    s.send('bob') #Tells KDC Alice wants to talk with bob
    encryptedArr = []
    encryptedArr.append(s.recv(1024))
    s.send('a') 
    encryptedArr.append(s.recv(1024))
    s.send('a')
    arr = []
    for i in encryptedArr:
        arr.append(des.decrypt(i, ka))
    sessionKey = ''.join(str(e) for e in arr[0])
    sessionKey += str(arr[1][0]) + str(arr[1][1]) #Groups session key

    encryptedEnv = [] #Decrypt encrypted envelope to get bob's envelope
    encryptedEnv.append(s.recv(1024))
    s.send('a')
    encryptedEnv.append(s.recv(1024))
    s.send('a')
    arr = []
    for i in encryptedEnv:
        arr.append(des.decrypt(i, ka))
    
    encryptedtimeArr = [] #Getting the timestamp
    while(True):
        t = s.recv(1024)
        if(t == 'done'):
            break
        else:
            encryptedtimeArr.append(t)
            s.send('a')
    timeArr = []
    for i in encryptedtimeArr:
        timeArr.append(des.decrypt(i, ka))
    s.close()
    break

raw_input('press enter')
'''
This is the final part where Alice contacts Bob and gives him the timestamp to prevent replay attacks. Alice also sends the envelope with bob's
copy of the session key. Bob will decrypt and send a nounce to Alice to prove she is who she says.
'''
s = socket.socket()
s.connect((host, 12345))
while(True):
    #timestamp
    for i in timeArr:
        tmp = ''.join(str(e) for e in i)
        s.send(tmp)
        s.recv(1024)
    s.send('done')
    for i in arr:
        tmp = ''.join(str(e) for e in i)
        s.send(tmp)
        s.recv(1024)
    
    encryptedNonce = s.recv(1024) #Getting the Nonce from bob to prove this is Alice
    nonce = des.decrypt(encryptedNonce, sessionKey)
    newNonce = ''.join(str(e) for e in nonce)
    newNonce = bin(int(newNonce, 2) -3)[2:] #Simple math operation on the nonce
    encryptedNonce = des.encrypt(newNonce, sessionKey)
    encryptedNonce = ''.join(str(e) for e in encryptedNonce)
    s.send(encryptedNonce) #Send back
    s.close() #could keep going now that they are secured but won't
    break
    