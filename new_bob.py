#!/usr/bin/python3

import socket
import time
import des
import numpy as np

if __name__ == '__main__':
    try:
        privKey = '1111000000'
        bob = socket.socket()
        bob.bind(('', 12345))
        bob.listen(5)
        while True:
            c, addr = bob.accept()

            encryptedSessionkey1 = c.recv(1024).decode()
            c.send('ack'.encode())
            encryptedSessionkey1 = np.array(list(encryptedSessionkey1))
            tmp = des.decrypt(encryptedSessionkey1, privKey)
            sessionKey1 = ''.join(str(x) for x in tmp)
            encryptedSessionkey2 = c.recv(1024).decode()
            c.send('ack'.encode())
            encryptedSessionkey2 = np.array(list(encryptedSessionkey2))
            tmp = des.decrypt(encryptedSessionkey2, privKey)
            sessionKey2 = ''.join(str(x) for x in tmp)
            sessionKey2 = sessionKey2[6:]
            sessionKey = sessionKey1+sessionKey2

            encryptedPad = c.recv(1024).decode()
            print('Encrypted Pad: %s' %(encryptedPad))
            c.send('ack'.encode())
            encryptedTimestamp = c.recv(1024).decode().strip()
            print(encryptedTimestamp)
            c.send('ack'.encode())
            encryptedPadArr = np.array(list(encryptedPad))
            print(encryptedPadArr)
            padArr = des.decrypt(encryptedPadArr, sessionKey)
            pad = ''.join(str(x) for x in padArr)
            pad = int(pad,2)
            print(pad)
            timestamp = ''

            print(encryptedTimestamp)
            for i in range(0,int(len(encryptedTimestamp)/8)):
                tmpStrArr = np.array(list(encryptedTimestamp[i*8:(i+1)*8]))
                print('tmpArr: %s' %(tmpStrArr))
                tmpStrArr = des.decrypt(tmpStrArr, sessionKey)
                tmpStr = ''.join(str(x) for x in tmpStrArr)
                timestamp += tmpStr
            for i in range(0,pad+1):
                timestamp = timestamp[:len(timestamp)]
            print('Original timestamp: %s' %(timestamp))
            intTimestamp = int(timestamp,2)
            if(int(time.time())-intTimestamp > 1000):
                print('too old')
                bob.close()
                break
            nonce = ['0','0','0','0','0','1','1','1']
            encryptedNonceArr = des.encrypt(nonce, sessionKey)
            encryptedNonce = ''.join(str(x) for x in encryptedNonceArr)
            c.send(encryptedNonce.encode())
            encryptedNonce = c.recv(1024).decode()

            encryptedNonceArr = np.array(list(encryptedNonce))
            nonceArr = des.decrypt(encryptedNonceArr, sessionKey)
            nonce = int(''.join(str(x) for x in nonceArr), 2)
            if(nonce-1 != 7):
                print('invalid nonce')
                bob.close()
                break
            else:
                print('valid nonce time to chat')
            
            while(True):
                encryptedMsg = c.recv(1024).decode()
                encryptedMsgArr = np.array(list(encryptedMsg))
                msgArr = des.decrypt(encryptedMsgArr, sessionKey)
                msg = ''.join(str(x) for x in msgArr)
                print(msg)


    except KeyboardInterrupt:
        bob.close()