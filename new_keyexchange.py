#!/usr/bin/python3

import socket

def computationalDiffieHellman(sock, g, n, secret):
    publicOperation = pow(g, secret, n)
    sock.send(str(publicOperation).encode())
    print(publicOperation)
    partyPublicOperation = int(sock.recv(1024).decode())
    return pow(partyPublicOperation, secret, n)
    