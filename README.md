# Homework2.b

Network Security Homework 2.b

Let me start by saying this homework was sloppy and the code may be hard to follow but I need to submit it before midnight so just follow my readme.

This project implemented Needham-Schroeder's protocol with timestamps to prevent replay attacks. This project also used Diffie-Hellman key exchange to connect to a Key Distribution Center(KDC). The timestamps were used to make sure someone wasn't using an old session key to talk with Bob or Alice and the Diffie-Hellman was used so Alice and Bob could give the KDC their keys securely in an otherwise unsecure channel.

A big challenge I had to overcome when working on this project was the size of strings that I had to encrypt/decrypt. I used the TOY DES with the keys to encrypt/decrypt but that only takes 8bit messages and 10bit keys. My solution was to only use 10bit keys which was easy but breaking up messages was the hard part. My solution was to pad 0's to make it fit the 8bit size if a segment wasn't long enough. But as I write this right now I can see I padded on the wrong end, and do not have time to fix right now.

The process of this goes as follows:
- bob establishes a public key with KDC using diffie-hellman and sends his key securely
- Alice establishes a public key with KDC using diffie-hellman and sends her key securely
- Alice contacts the KDC saying I want to talk to Bob
- The KDC sends Alice the following:
- - A timestamp to make sure the session is valid encrypted with Bob's private key and then her private key
- - An encrypted copy of the session key encrypted with her private key
- - An encrypted copy of the session key encrypted with Bob's private key and then with her private key
- Alice then decrypts all 3 of these things with her private key and gains the following:
- - A session key
- - An encrypted timestamp for Bob
- - An encrypted session key for Bob
- Alice then makes contact with Bob
- Alice then sends Bob the following:
- - A timestamp encrypted with Bob's private key
- - An envelope encrypted with Bob's private key containing the session key
- Bob then decrypts the timestamp and makes sure it hasn't expired (I gave the session key 1 hour until it expires)
- Bob then decrypts his envelope to get himself his own session key

Now Alice and Bob both have a session key and hopefully it isn't expired. Now Bob has to make sure he is actually communicating with Alice (almost a redundancy). The process of figuring out if Alice is who she says she is goes as following:
- Bob creates a nonce (a random bit string)
- Bob encrypts the nonce with the session key
- Bob sends the encrypted nonce to Alice
- Alice decrypts the nonce and then does some simple arithmetic on the nonce (This is so Bob knows she actually decrypts it) (For my project I just subtracted 3)
- Alice then encrypts the altered nonce and sends it back to Bob
- Bob decrypts the nonce and does the reverse of the arithmetic to see if the altered nonce is the nonce (For my project just add 3 back)
- If the two nonces are the same when reversed then the connection is secured. Alice and Bob have a valid session key and Bob is actually talking to Alice

From This point Alice and Bob can communicate using the session key but for my project I just closed the connection.



I made some assumptions in my code that might not fly in the real world.

- I assumed the end systems knew how much they were receiving or what the message was to stop receiving.

In the real world no one would know how long a message might be or what the key word was to stop receiving messages

- I also assumed the padding would be ok.

I'm worried that in the real world padding 0's might become predictable and show some hints to cracking my cryptosystem

- I also made it a very structured communication.

You have to first run KDC.py then Bob.py then Alice.py. In the real world the KDC should always be on and Alice and Bob shouldn't be like a client-server relationship but more of a peer-to-peer

- I also sent a dummy response everytime a message was received.

For some reason if there wasn't a response the received messages would get stacked and appear weird. This could be fixed if I had more time.


To execute the following project:
- Have 3 terminals open
- run new_KDC.py
- run new_bob.py
- run new_alice.py
- crt+c to leave the chatroom