#!/usr/bin/python3

import socket
import new_keyexchange as keyExchange
import des
import numpy as np

if __name__ == '__main__':
    try:
        binPrivateSecret = '110011010'
        intPrivateSecret = int(binPrivateSecret,2)
        aliceToKDC = socket.socket()
        aliceToKDC.connect(('127.0.0.1', 55555))
        g = 3
        n = 1000
        aliceToKDC.send(str(g).encode())
        aliceToKDC.recv(3)
        aliceToKDC.send(str(n).encode())
        aliceToKDC.recv(3)
        KDCKey = keyExchange.computationalDiffieHellman(aliceToKDC, g, n, intPrivateSecret)
        KDCKey = bin(KDCKey)[2:]
        while(len(KDCKey)>10):
            print(KDCKey)
            KDCKey = KDCKey[1:]
        print(KDCKey)

        encryptedPad = aliceToKDC.recv(1024).decode()
        print('Encrypted Pad: %s' %(encryptedPad))
        aliceToKDC.send('ack'.encode())
        encryptedTimestamp = aliceToKDC.recv(1024).decode().strip()
        print(encryptedTimestamp)
        aliceToKDC.send('ack'.encode())
        encryptedPadArr = np.array(list(encryptedPad))
        print(encryptedPadArr)
        padArr = des.decrypt(encryptedPadArr, KDCKey)
        pad = ''.join(str(x) for x in padArr)
        pad = int(pad,2)
        print(pad)
        timestamp = ''

        print(encryptedTimestamp)
        for i in range(0,int(len(encryptedTimestamp)/8)):
            tmpStrArr = np.array(list(encryptedTimestamp[i*8:(i+1)*8]))
            print('tmpArr: %s' %(tmpStrArr))
            tmpStrArr = des.decrypt(tmpStrArr, KDCKey)
            tmpStr = ''.join(str(x) for x in tmpStrArr)
            timestamp += tmpStr
        for i in range(0,pad+1):
            timestamp = timestamp[:len(timestamp)]
        print('Original timestamp: %s' %(timestamp))
        
        encryptedSessionKey1 = aliceToKDC.recv(1024).decode()
        print('encryptedSessionKey1: %s' %(encryptedSessionKey1))
        aliceToKDC.send('ack'.encode())
        encryptedSessionKey1 = np.array(list(encryptedSessionKey1))
        tmp = des.decrypt(encryptedSessionKey1, KDCKey)
        SessionKey1 = ''.join(str(x) for x in tmp)
        encryptedSessionKey2 = aliceToKDC.recv(1024).decode()
        aliceToKDC.send('ack'.encode())
        encryptedSessionKey2 = np.array(list(encryptedSessionKey2))
        tmp = des.decrypt(encryptedSessionKey2, KDCKey)
        SessionKey2 = ''.join(str(x) for x in tmp)
        SessionKey2 = SessionKey2[6:]
        sessionKey = SessionKey1+SessionKey2

        print('sessionKey: %s' %(sessionKey))
        encryptedBobEnvelope1 = aliceToKDC.recv(1024).decode()
        aliceToKDC.send('ack'.encode())
        encryptedBobEnvelope1 = np.array(list(encryptedBobEnvelope1))
        tmp = des.decrypt(encryptedBobEnvelope1, KDCKey)
        bobEnvelope1 = ''.join(str(x) for x in tmp)
        encryptedBobEnvelope2 = aliceToKDC.recv(1024).decode()
        aliceToKDC.send('ack'.encode())
        encryptedBobEnvelope2 = np.array(list(encryptedBobEnvelope2))
        tmp = des.decrypt(encryptedBobEnvelope2, KDCKey)
        bobEnvelope2 = ''.join(str(x) for x in tmp)

        aliceToKDC.close()

        print(sessionKey)

        aliceToBob = socket.socket()
        aliceToBob.connect(('127.0.0.1', 12345))
        
        aliceToBob.send(bobEnvelope1.encode())
        aliceToBob.recv(3)
        aliceToBob.send(bobEnvelope2.encode())
        aliceToBob.recv(3)


        print('Original timestamp: %s' %(timestamp))
        encryptedTimeStamp = ''
        print('timestamp: %s' %(timestamp))
        for i in range(0,int(len(timestamp)/8)):
            print('i: %s' %(i))
            tmpStrArr = np.array(list(timestamp[i*8:(i+1)*8]))
            tmpStrArr = des.encrypt(tmpStrArr, sessionKey)
            tmpStr = ''.join(str(x) for x in tmpStrArr)
            encryptedTimeStamp += tmpStr
        if(len(timestamp) % 8 != 0):
            pad = bin(len(timestamp)%8)[2:]
            tmpStr = timestamp[len(timestamp)-(len(timestamp)%8):]
            while(len(tmpStr)<8):
                tmpStr += '0'
            tmpStrArr = np.array(list(tmpStr))
            print('here: %s' %(tmpStrArr))
            tmpStrArr = des.encrypt(tmpStrArr, sessionKey)
            tmpStr = ''.join(str(x) for x in tmpStrArr)
            encryptedTimeStamp += tmpStr
            print('what' + encryptedTimeStamp)

            while(len(pad)< 8):
                pad = '0' + pad
            padArr = np.array(list(pad))
            print(padArr)
            encryptedPadArr = des.encrypt(padArr, sessionKey)
            encryptedPad = ''.join(str(x) for x in encryptedPadArr)
            print('encrypt'+encryptedPad)
            aliceToBob.send(encryptedPad.encode())
            aliceToBob.recv(3)
            print('Pad sent: %s' %(encryptedPad))
        else:
            tmp = des.encrypt(['0','0','0','0','0','0','0','0'], sessionKey)
            aliceToBob.send((''.join(str(x) for x in tmp).encode()))
            aliceToBob.recv(3)
        aliceToBob.send(encryptedTimeStamp.encode())
        aliceToBob.recv(3)
        print('timestamp sent: %s' %(encryptedTimeStamp))
        

        encryptedNonce = aliceToBob.recv(1024).decode()
        encryptedNonceArr = np.array(list(encryptedNonce))
        nonceArr = des.decrypt(encryptedNonce, sessionKey)
        nonce = int(''.join(str(x) for x in nonceArr),2)
        nonce += 1
        nonce = bin(nonce)[2:]
        while(len(nonce)<8):
            nonce = '0'+ nonce
        nonceArr = np.array(list(nonce))
        encryptedNonceArr = des.encrypt(nonceArr, sessionKey)
        encryptedNonce = ''.join(str(x) for x in encryptedNonceArr)
        aliceToBob.send(encryptedNonce.encode())

        while True:
            msg = input('Enter 8 bit binary message: ')
            msgArr = np.array(list(msg))
            encryptedMsgArr = des.encrypt(msgArr, sessionKey)
            encryptedMsg = ''.join(str(x) for x in encryptedMsgArr)
            aliceToBob.send(encryptedMsg.encode())
    except KeyboardInterrupt:
        aliceToKDC.close()